extends KinematicBody2D

const MOTION_SPEED = 200
var motion = Vector2()
var hachaScene = preload("res://Hacha.tscn")

func _physics_process(delta):
	var animatedSprite = $VickillerAnimatedSprite
	
	if Input.is_action_pressed("ui_up"):
		motion = Vector2(0, -1)
		animatedSprite.play("walking_up")
	elif Input.is_action_pressed("ui_down"):
		motion = Vector2(0, 1)
		animatedSprite.play("walking_down")
	elif Input.is_action_pressed("ui_right"):
		motion = Vector2(1, 0)
		animatedSprite.flip_h = false
		animatedSprite.play("walking_side")
	elif Input.is_action_pressed("ui_left"):
		motion = Vector2(-1, 0)
		animatedSprite.flip_h = true
		animatedSprite.play("walking_side")
	else:
		animatedSprite.play("idle")
		motion = Vector2(0, 0)

	if Input.is_action_just_pressed("ui_shoot") && motion != Vector2(0, 0):
		var hacha = hachaScene.instance()
		hacha.shoot_in_position_with_direction(self.position, motion)
		get_parent().add_child(hacha)
		pass
		
	motion = motion.normalized() * MOTION_SPEED	
	move_and_slide(motion)
	
	pass

