extends KinematicBody2D

# class member variables go here, for example:
var esEnemigo = true
var tipoId = 0
var isCheater = false
var horasTipo = 0
var direction = null
var speed = 3
var iterations_before_direction_change = 0
var max_iterations = 200
var min_iterations = 20
var derecha = Vector2(-1,0)
var izquierda = Vector2(1,0)
var arriba = Vector2(0,-1)
var abajo = Vector2(0,1)

func _ready():
	tipoId = (randi() % 3) + 1
	isCheater = randi() % 2 == 1 
	$Lalo.hide()
	$Pablo.hide()
	$Feche.hide()
	
	var color = null
	if tipoId == 1:
		$Lalo.show()
		horasTipo = 8
		color = Color(0,0,255)
	if tipoId == 2:
		$Pablo.show()
		horasTipo = 7
		color = Color(255,0,0)
	if tipoId == 3:
		$Feche.show()
		horasTipo = 6
		color = Color(0,255,0)
	var modificador = 0
	if isCheater:
		modificador = (randi() % (horasTipo - 1)) + 1
	
	var dias = [20,21,22,23][randi() % 4]
	
	$HorasLabel.set("custom_colors/font_color", color)
	$HorasLabel.text = str(horasTipo * dias + modificador)
	
	set_random_direction()
	
func recibirHachazo():
	get_parent().mate_enemigo(self)
	
func me_fui():	
	get_parent().se_fue_un_enemigo(self)
	
func _process(delta):
	iterations_before_direction_change = iterations_before_direction_change - 1
	if iterations_before_direction_change == 0:
		set_random_direction()
	var collision = move_and_collide(direction * speed)
	if collision:
		change_direction()
	if get_parent().get_node("SalidaArea").overlaps_body(self):
		me_fui()
		
func set_random_direction():

	direction = [derecha,izquierda,arriba,abajo,izquierda,izquierda,izquierda,abajo, abajo, abajo][(randi() % 10)]
	iterations_before_direction_change = (randi() % (max_iterations - min_iterations)) + min_iterations

func change_direction():
	if direction == derecha:
		direction = arriba
	if direction == arriba:
		direction = izquierda
	if direction == izquierda:
		direction = abajo
	if direction == abajo:
		direction = derecha					