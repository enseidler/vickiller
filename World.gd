extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var enemigoScene = preload("res://Enemigo.tscn")
var vidas = 3
var isGameOver = false
var tiempo_inicial = null
var max_enemies = 5 
var enemy_count = 0 
var probability_spawn = 20

func _ready():
	randomize()
	spawmear_nuevo_enemigo()
	$GameOverOverlay.hide()
	$SalidaArea.set("es_salida", true)
	tiempo_inicial = OS.get_ticks_msec()
	actualizar_timer()
	
func actualizar_timer():
	if !isGameOver:
		$TimerLabel.text = str(float(OS.get_ticks_msec() - tiempo_inicial)/1000.0)
	else:
		$GameOverOverlay/ScoreTimer.text = $TimerLabel.text

func spawmear_nuevo_enemigo():
	var spawm = $SpawnPoints.get_child(randi() % $SpawnPoints.get_child_count()) 
	var enemigo = enemigoScene.instance()
	self.add_child(enemigo)
	enemigo.position = spawm.global_position
	enemy_count = enemy_count + 1

func maybe_spawner_nuevo_enemigo():
	if enemy_count < max_enemies:
		if probability_spawn > (randi() % 10000):
			spawmear_nuevo_enemigo()
		
func _process(delta):
	if Input.is_action_just_pressed("ui_shoot") && isGameOver:
		get_tree().reload_current_scene()
	actualizar_timer()
	maybe_spawner_nuevo_enemigo()
	
func perder_vida():
	if vidas == 3:
		$Life3.hide()
	if vidas == 2:
		$Life2.hide()
	if vidas == 1:
		$Life1.hide()
		show_game_over()
	vidas = vidas - 1 
		
func show_game_over():
	isGameOver = true
	remove_child($Vickiller)
	$GameOverOverlay.show()
	
	
func se_fue_un_enemigo(enemigo):
	if enemigo.isCheater:
		perder_vida()
	enemy_count = enemy_count - 1
	remove_child(enemigo)

func mate_enemigo(enemigo):
	if !enemigo.isCheater:
		perder_vida()
	enemy_count = enemy_count - 1
	remove_child(enemigo)		
