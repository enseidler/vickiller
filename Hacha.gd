extends KinematicBody2D

# class member variables go here, for example:
var direction = Vector2(0,0)
var speed = 12
var initialDistance = 80
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	get_viewport().audio_listener_enable_2d = true
	$HachaSprite/AudioStreamPlayer2D.play()
	pass

func _process(delta):
	$HachaSprite.rotation = $HachaSprite.rotation + 0.5
	var kinematic_collision = move_and_collide(self.direction * speed)
	if kinematic_collision:
		if typeof(kinematic_collision.collider) == TYPE_OBJECT && kinematic_collision.collider.is_class("StaticBody2D"):
			get_parent().remove_child(self)
		if kinematic_collision.collider.get("esEnemigo"):
			get_parent().remove_child(self)
			kinematic_collision.collider.recibirHachazo()
func shoot_in_position_with_direction(pos, dir):
	self.position = pos + dir * initialDistance
	self.direction = dir
		